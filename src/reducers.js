import {combineReducers} from "redux";
import noteReducer from "./note/reducers/noteReducer";

const reducers = combineReducers({
	note: noteReducer
});
export default reducers;
