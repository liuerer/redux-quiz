import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./note/componets/header/Header";
import {Switch, Route} from "react-router";
import Home from "./note/pages/home/Home";
import NoteDetails from "./note/pages/noteDetails/NoteDetails";
import CreateNote from "./note/pages/create/CreateNote";

class App extends Component {
	render() {
		return (
			<Router>
				<div className='App'>
					<Header />
					<Switch>
						<Route exact path={'/notes/create'} component={CreateNote} />
						<Route exact path={'/notes/:id(\\d+)'} component={NoteDetails} />
						<Route exact path={'/'} component={Home} />
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App;
