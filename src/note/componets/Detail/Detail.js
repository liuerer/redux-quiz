import React from 'react';
import {Link} from "react-router-dom";
import './Detail.less'
import ReactMarkdown from "react-markdown";

class Detail extends React.Component{

	render() {
		const {id, title, description} = this.props.noteDetailInfo;
		const deleteNoteMethod = this.props.deleteNote;
		return (
			<div className={'note-detail-info'}>
				<h2>{title}</h2>
				<hr/>
				<ReactMarkdown source={description}/>
				<hr/>
				<div className={'buttons'}>
					<Link to={'/'}><button className={'delete'} onClick={()=>{deleteNoteMethod(id)}}>删除</button></Link>
					<Link to={'/'}><button className={'back-home'}>返回</button></Link>
				</div>
			</div>
		)
	}
}

export default Detail;
