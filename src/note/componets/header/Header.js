import React from 'react';
import { MdEventNote } from "react-icons/md";
import "./Header.less"
import { Link }from "react-router-dom";

const Header = () => {
	return (
		<header className='home-header'>
			<Link to='/notes/create'><MdEventNote /></Link>
			<h1>NOTES</h1>
		</header>
	);
};

export default Header;
