import React from 'react'
import {Link} from "react-router-dom";
import {MdLibraryAdd} from "react-icons/md";
import './NotesList.less'

class NotesList extends React.Component {
	render() {
		const notesList = Object.keys(this.props.notes).map((item) => {
			const {id, title} = this.props.notes[item];
			const path = `/notes/${id}`;
			return (
				<Link to={path}><li key={id.toString()}>{title}</li></Link>
			);
		});
		return (
			<ul className={'notes-list'}>
				{notesList}
				<Link to={'/notes/create'}><li><MdLibraryAdd/></li></Link>
			</ul>
		);
	}
}

export default NotesList;
