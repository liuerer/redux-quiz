import React from 'react'
import {Link} from "react-router-dom";
import './SideBar.less'

class SideBar extends React.Component{

	render() {
		const notesList = Object.keys(this.props.notes).map((item) => {
			const {id, title} = this.props.notes[item];
			const path = `/notes/${id}`;
			return (
				<Link to={path}>
					<li className={'nav-link'} key={id.toString()}>{title}</li>
				</Link>
			);
		});
		return(
			<ul className={'side-bar'}>
				{notesList}
			</ul>
		);
	}
}

export default SideBar;
