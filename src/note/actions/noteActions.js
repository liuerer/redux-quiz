
export const getNotes = () => (dispatch) => {
	fetch('http://localhost:8080/api/posts')
		.then(response=>response.json())
		.then(result => {
			dispatch({
				type: 'GET_NOTES',
				notes: result
			});
		})
		.catch(alert);
};

export const deleteNote = (id, callback) => (dispatch) => {
	fetch(`http://localhost:8080/api/posts/${id}`, {method: 'DELETE'})
		.then(() => {
			dispatch({
				type: 'DELETE_NOTE'
			});
		})
		.then(callback())
		.catch(callback());
};

export const createNote = (content, callback) => () => {
	fetch('http://localhost:8080/api/posts',
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
			body: JSON.stringify(content)
		})
		.then(callback())
		.catch(alert);
};
