import React from 'react'
import {Link} from "react-router-dom";
import {createNote, getNotes} from "../../actions/noteActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import './CreateNote.less'


class CreateNote extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			title: '',
			description: ''
		}
	}

	handleOnChangeTitle(event) {
		this.setState({
			title: event.target.value
		});
	}

	handleOnChangeDescription(event) {
		this.setState({
			description: event.target.value
		});
	}

	handleOnClickSubmit() {
		this.props.createNote(this.state, this.props.getNotes);
	}

	render() {
		const {title, description} = this.state;
		return (
			<div className={'creat-note'}>
				<h2>创建笔记</h2>
				<hr/>
				<label>标题</label>
				<input className={'title-input'} value={title} onChange={this.handleOnChangeTitle.bind(this)}/>
				<label>正文</label>
				<textarea className={'description-input'} value={description}
				          onChange={this.handleOnChangeDescription.bind(this)}/>
				<section className={'buttons'}>
					<Link to={'/'}>
						<button className={'submit'}
						        disabled={title === '' || description === ''}
						        onClick={this.handleOnClickSubmit.bind(this)}>提交
						</button>
					</Link>
					<Link to={'/'}>
						<button className={'cancel'}>取消</button>
					</Link>
				</section>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({
	getNotes,
	createNote
}, dispatch);

export default connect(undefined, mapDispatchToProps)(CreateNote);
