import React from 'react'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getNotes} from "../../actions/noteActions";
import NotesList from "../../componets/notedList/NotesList";

class Home extends React.Component{

	componentDidMount() {
		this.props.getNotes();
	}

	render() {
		return (
			<div className = 'home-page'>
					<NotesList notes={this.props.notes}/>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	notes: state.note.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
