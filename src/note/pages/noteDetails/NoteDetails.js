import React from 'react'
import SideBar from "../../componets/sideBar/SideBar";
import {bindActionCreators} from "redux";
import {deleteNote, getNotes} from "../../actions/noteActions";
import {connect} from "react-redux";
import Detail from "../../componets/Detail/Detail";
import './NoteDetails.less'

class NoteDetails extends React.Component{

	componentDidMount() {
		this.props.getNotes();
	}

	handleOnClick(id) {
		this.props.deleteNote(id, this.props.getNotes);
	}

	render() {
		const noteId = this.props.match.params.id;
		const notes = this.props.notes;
		let note = null;
		if(notes !== undefined){
			for(let i=0; i<notes.length; ++i){
				if(notes[i].id === Number(noteId)){
					note = notes[i];
					break;
				}
			}
		}
		return(
			<div className={'note-details'}>
				<SideBar notes={this.props.notes}/>
				{(note !== null) && <Detail noteDetailInfo={note} deleteNote={this.handleOnClick.bind(this)}/>}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	notes: state.note.notes
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
	getNotes,
	deleteNote
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetails);
